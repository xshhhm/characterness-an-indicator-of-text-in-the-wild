characterness
=============

This software package contains the scene text detection algorithm described in the the paper: "Characterness: An Indicator of Text in the Wild", IEEE Transactions on Image Processing. 

This software is provided for research purposes. Please cite our paper if you use the package. The code is tested on Matlab 2013a, Ubuntu 13.10. 

Installation
-------------

Before you use this software, please download the vlfeat toolbox from 
http://www.vlfeat.org/ and add it to the current Matlab search path. 

Getting started
---------------

Please run the demo.m file to see how the algorithm works. 

Note that in the package, we load pretained distributions of the three cues on characters and non-characters (See data/CueNegativeDistribution_229.mat and data/CuePositiveDistribution_229.mat).The distributions are learned from the ICDAR 2013 Robust Reading Competition (Challenge 2) training set. 


License
-------
Copyright (c) 2013 Yao Li, Wenjing Jia, Chunhua Shen, Anton van den Hengel

This code is released under the [Mozilla public license 2.0](http://www.mozilla.org/MPL/2.0/).


Questions and Comments
----------------------

If you have comments/questions, please email to 
yao.li01@adelaide.edu.au, chunhua.shen@adelaide.edu.au.

